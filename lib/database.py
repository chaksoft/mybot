#-*- coding:utf-8 -*-

from abc import ABCMeta, abstractmethod
import random

class AbstractDatabase(metaclass=ABCMeta):
	"""
	Abstract database object

	Must be inherited and implement some base data querying methods
	"""
	@abstractmethod
	def post(self, entity, data, **criteria):
		"""
		Create or update a data object
		In a creation, criteria will be empty.

		@return The created object ID in the database
		"""
		pass

	@abstractmethod
	def get(self, entity, sort=None, **criteria):
		"""
		Get all data objects in the entity (table for RDBS, collection for NoSQL)
		The result set can be sorted with the `sort` argument, and
			can be reduced by the list of criteria

		Sort must be an array of tuples :
		For example to order result by field 'foo' descending :
			sort=[('foo', -1)]
		To order by field 'bar' ascending :
			sort=[('bar', 1)]
		And both :
			sort=[('foo', -1), ('bar', 1)]

		@return An array of dict objects containing the result set properties
		"""
		pass

	@abstractmethod
	def get_one(self, entity, sort=None, **criteria):
		"""
		Get only the first result set object in the given entity
			according to the `sort` argument and the list of criteria

		@return A dict object with the returned result set properties
		"""
		pass

	@abstractmethod
	def count(self, entity, **criteria):
		"""
		Count all objects in the entity according to the list of criteria

		@return Found records count
		"""
		pass

	@abstractmethod
	def delete(self, entity, **criteria):
		"""
		Delete an object in the entity according to the list of the criteria

		@return Number of deleted records
		"""
		pass

class DataTransformer:
	"""
	This object imports the good database wrapper and relays querys from DataManager.
	"""
	def __init__(self, config):
		"""
		Creats the database wrapper for this object according to the given configuration
		"""
		if config is not None and config.engine != "none":
			exec('from mybot.lib.db import {} as datalib'.format(config.engine))
			self._db = getattr(locals()['datalib'], "{}{}".format(config.engine.title(), "Database"))(config)

	def table_name(self, module, name):
		"""
		Generates table name
		"""
		return "{}_{}".format(module, name)

	def save_object(self, module, _obj):
		"""
		Saves object in the database
		"""
		if _obj.id() is None:
			return self._db.post(self.table_name(module, _obj._name_), _obj.to_json())
		else:
			return self._db.post(self.table_name(module, _obj._name_), _obj.to_json(), _id=_obj.id())

	def get_objects(self, module, name, one=False, sort=None, **criteria):
		"""
		Get objects from database
		"""
		if not one:
			return self._db.get(self.table_name(module, name), sort=sort, **criteria)
		else:
			return self._db.get_one(self.table_name(module, name), sort=sort, **criteria)

	def get_object_count(self, module, name, **criteria):
		"""
		Gets objects count
		"""
		return self._db.count(self.table_name(module, name), **criteria)

	def delete_object(self, module, name, **criteria):
		"""
		Delets an object in the database
		"""
		return self._db.delete(self.table_name(module, name), **criteria)

	def get_random(self, module, name, **criteria):
		"""
		Gets a random object from the database
		"""
		_c = self.get_object_count(module, name, **criteria)
		if _c > 0:
			_x = random.randrange(0, _c)
			_ids = [ c['_id'] for c in self.get_objects(module, name, **criteria) ]
			for i in range(_c):
				if i == _x:
					return self.get_objects(module, name, one=True, _id=_ids[i])
		return None
 