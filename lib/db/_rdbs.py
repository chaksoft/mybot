#-*- coding:utf-8 -*-

from mybot.lib.database import AbstractDatabase

class RDBSDatabase(AbstractDatabase):
	"""
	Generic database implementation for RDBS

	Please inherit this class to create a new database wrapper
	You can add some methods here to give some additional helpers
	"""
	def __init__(self, engine, config):
		"""
		Initialize engine with the engine class given as `engine` argument
		"""
		try:
			self.client = engine.connect(
				host=config.host,
				port=config.port,
				user=config.user,
				passwd=config.password,
				db=config.name,
				charset="utf8",
				use_unicode=True
			)
		except AttributeError as ae:
			raise Exception("Please check your configuration fields in '_db_' section of the 'config.json' file. Something is missing.")
		self.client.autocommit(True)
		self.cursor = self.client.cursor()

	def close(self):
		"""
		Close active cursor and database engine
		"""
		if hasattr(self, 'cursor') and self.cursor is not None:
			self.cursor.close()
		if hasattr(self, 'client') and self.client is not None:
			self.client.close()

	def __fetch_result(self, dbapi_result):
		"""
		Inner method for fetching results from a SELECT
		"""
		cols = self.cursor.description
		result = []
		for value in dbapi_result:
			_fr = {}
			for (index, column) in enumerate(value):
				_fr[cols[index][0]] = column
			result.append(_fr)
		if len(result) == 0:
			return None
		return result

	def post(self, entity, data, **criteria):
		"""
		Update or insert data into the database
		"""
		if len(criteria.keys() == 0):
			rq = "INSERT INTO {} ({}) VALUES ({})".format(
				entity,
				", ".join(data.keys()),
				"'" + "', '".join([data[k] for k in data]) + "'"
			)
		else:
			rq = "UPDATE {} SET {} WHERE {}".format(
				entity,
				", ".join(["{} = '{}'".format(k, data[k]) for k in data]),
				" AND ".join(["{} = '{}'".format(c, criteria[c]) for c in criteria])
			)
		return self.cursor.execute(rq)

	def get(self, entity, sort=None, **criteria):
		"""
		Get data from the database according to the given criteria
		"""
		rq = "SELECT * FROM {} ".format(entity)
		if len(criteria.keys()) > 0:
			rq += " WHERE " + " AND ".join(["{} = '{}'".format(c, criteria[c] for c in criteria]))
		if sort is not None:
			rq += " ORDER BY {}".format(
				", ".join(["{} {}".format(s[0], "ASC" if s[1] > 0 else "DESC") for s in sort])
			)
		self.cursor.execute(rq)
		_all = self.cursor.fetchall()
		return self.__fetch_result(_all)

	def get_one(self, entity, sort=None, **criteria):
		"""
		Get the first record of the result set according to the given criteria
		"""
		_all = self.get(entity, sort=sort, **criteria)
		if _all is not None:
			return _all[0]
		return None

	def count(self, entity, **criteria):
		"""
		Count records of the result set according to the given criteria
		"""
		rq = "SELECT COUNT(1) FROM {}".format(entity)
		if len(criteria.keys()) > 0:
			rq += " WHERE " + " AND ".join(["{} = '{}'".format(c, criteria[c]) for c in criteria])
		self.cursor.execute(rq)
		return self.cursor.fetchone()[0]

	def delete(self, entity, **criteria):
		"""
		Delete a record according to the given criteria
		"""
		rq = "DELETE FROM {}".format(entity)
		if len(criteria.keys()) > 0:
			rq += " WHERE " + " AND ".join(["{} = '{}'".format(c, criteria[c]) for c in criteria])
		return self.cursor.execute(rq)
