#-*- coding:utf-8 -*-

from mybot.lib.database import AbstractDatabase
try:
	from pymongo import MongoClient
except ImportError as ie:
	raise Exception("You must install 'pymongo' package to use MongoDB wrapper for MyBot.")


class MongoDatabase(AbstractDatabase):
	"""
	Wrapper for MongoDB database interactivity
	Inherits abstract class AbstractDatabase

	Configuration needed:
		- host: Database server IP address or hostname
		- port: Database port
		- name: Database collection name
		- user: Database username (optional)
		- pwd : Database user password (optional)
	"""
	def __init__(self, config):
		fulluri = "%s:%d" % (config.host, config.port)
		if hasattr(config, 'user') and hasattr(config, 'pwd'):
			fulluri = "%s:%s@%s" % (config.user, config.pwd, fulluri)
		self.client = MongoClient("mongodb://%s" % (fulluri,))
		self.db = self.client[config.name]

	def post(self, collection, data, **criteria):
		_coll = self.db[collection]
		if len(criteria.keys()) == 0:
			return _coll.insert_one(data).inserted_id
		else:
			return _coll.update_one(criteria, {"$set": data}, upsert=True).upserted_id

	def get(self, collection, sort=None, **criteria):
		_coll = self.db[collection]
		return _coll.find(criteria, sort=sort)

	def get_one(self, collection, sort=None, **criteria):
		_coll = self.db[collection]
		return _coll.find_one(criteria, sort=sort)

	def count(self, collection, **criteria):
		_coll = self.db[collection]
		return _coll.find(criteria).count()

	def delete(self, collection, **criteria):
		_coll = self.db[collection]
		return _coll.delete_many(criteria).deleted_count
