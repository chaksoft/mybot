#-*- coding:utf-8 -*-

class QuitException(Exception):
	def __init__(self, reason):
		super(QuitException, self).__init__("Quit from IRC: {}".format(reason))
