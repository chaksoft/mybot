#-*- coding:utf-8 -*-

class EventArgs:
	"""
	Event arguments wrapper

	@since 4.3
	"""
	def __init__(self, kwargs):
		for k in kwargs:
			setattr(self, k, kwargs[k])


class EventThrower:
	"""
	Event Thrower
	Must be inherited to throw events

	[!] This object is synchronous !
	This class is an utility wrapper for callbacks
	"""
	
	def __init__(self):
		self.__events = {}

	def on(self, event, callback, priority = 99):
		"""
		Bind an event to a callback with a specified priority

		Highest Priority: 0
		Lowest Priority: 99
		"""
		if priority > 99:
			priority = 99
		elif priority < 0:
			priority = 0
		if not event in self.__events:
			self.__events[event] = []
		self.__events[event].append({
			'callback': callback,
			'priority': priority,
			'enabled': True
		})
		self.__events[event] = sorted(self.__events[event], key=lambda x: x['priority'])
		return self

	def off(self, event):
		"""
		Unbind an event by destroying all associated callbacks
		"""
		if event in self.__events:
			del self.__events[event]
		return self

	def toggle(self, event):
		"""
		Toggle enable an event

		@since 5.0
		"""
		if event in self.__events:
			for evt in self.__events[event]:
				evt['enabled'] = not evt['enabled']

	def disable(self, event):
		"""
		Disable an event

		@since 5.0
		"""
		if event in self.__events:
			for evt in self.__events[event]:
				evt['enabled'] = False

	def enable(self, event):
		"""
		Enable an event

		@since 5.0
		"""
		if event in self.__events:
			for evt in self.__events[event]:
				evt['enabled'] = True

	def trigger(self, event, **kwargs):
		"""
		Trigger an event with inline parameters
		"""
		if event in self.__events:
			for evt in self.__events[event]:
				if evt['enabled'] == True:
					evt['callback'](self, EventArgs(kwargs))
