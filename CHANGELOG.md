# Changelog

-----------

## v5.2

### Version 5.2a

|  Type    |  Description   |
 ----------|----------------|
|_Enhancement_|Added YAML parser for configuration.|

## v5.1

### Version 5.1a

|  Type    |  Description   |
 ----------|----------------|
|_Fix_|Bug when there is no database section in the configuration file.|
|_Enhancement_|Added wrapper for data lists in the `DataObject` construction.|

## v5.0

### Version 5.0B

This release includes some bugfixes and light improvements:

|  Type    |  Description   |
 ----------|----------------|
|_Fix_|Bug when getting random data when there is no record in the result set.|
|_Enhancement_|Added wrapper for module configuration loader.


### Version 5.0A
This is the first official release of MyBot library.

------------

> Copyright © 2015 Michael Chacaton. All rights reserved.