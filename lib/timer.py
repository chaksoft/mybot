#-*- coding:utf-8 -*-

import threading


class BotTimer:
	"""
	Helper class to create timers in modules
	This class executes a callback at regular intervals

	@since 5.0
	"""
	def __init__(self, tempo, target, args = [], kwargs = {}):
		"""
		Initializes the timer

		tempo: interval in seconds
		target: callback
		args: arguments passed to the callback
		kwargs: extended arguments passed to the callback
		"""
		self._target = target
		self._args = args
		self._kwargs = kwargs
		self._tempo = tempo
		self._started = False
		
	def _run(self):
		"""
		Internal run method
		Calls the callback and start a new timer to run the next round
		"""
		self._timer = threading.Timer(self._tempo, self._run)
		self._timer.start()
		self._target(*self._args, **self._kwargs)
		
	def start(self):
		"""
		Starts the timer by running the first timer to run the first round
		"""
		self._started = True
		self._timer = threading.Timer(self._tempo, self._run)
		self._timer.start()
		
	def stop(self):
		"""
		Stop the timer if it has been started
		Otherwise, this method does nothing
		"""
		if self._started:
			self._started = False
			if self._timer:
				self._timer.cancel()
