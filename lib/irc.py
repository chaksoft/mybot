﻿#-*- coding:utf-8 -*-

from . import settings, net, debug, timer


class User:
	"""
	IRC User representation
	"""
	def __init__(self, nick, ident, host, mode = '', away = False):
		self._nick = nick
		self._ident = ident
		self._host = host
		self._mode = mode
		self._away = away

	def change_nick(self, newnick):
		"""
		Change user nickname
		"""
		self._nick = newnick

	def add_mode(self, mode):
		"""
		Add a mode to the user
		"""
		self._mode = self._mode + mode

	def remove_mode(self, mode):
		"""
		Remove a mode from the user
		"""
		if mode in self._mode:
			self._mode = self._mode.replace(mode, '')

	def set_away(self, away):
		"""
		Set the away status
		"""
		self._away = away


class Channel:
	"""
	IRC Channel representation
	Holds current users and channel modes
	"""
	def __init__(self, srv, name, key):
		self._name = name
		self._key = key
		self._users = {}
		self._mode = ''

	def add_mode(self, mode):
		"""
		Add channel mode
		"""
		if not mode in self._mode:
			self._mode = self._mode + mode

	def remove_mode(self, mode):
		"""
		Remove channel mode
		"""
		if mode in self._mode:
			self._mode = self._mode.replace(mode, '')

	def add_user(self, nick, ident, host, mode = ""):
		"""
		Add a user to the channel
		"""
		debug.debug('Adding new user to channel ' + self._name + ' : ' + nick + '!~' + ident + '@' + host)
		self._users[nick] = User(nick, ident, host, mode)

	def remove_user(self, nick):
		"""
		Remove a user from the channel
		"""
		if nick in self._users:
			del self._users[nick]

	def is_user_present(self, nick):
		"""
		Return True if the user identified by the given nickname is on the channel,
			return False otherwise.
		"""
		return nick in self._users

	def clear_users(self):
		"""
		Clear all users in the channel
		"""
		self._users = {}

	def change_user_nick(self, oldnick, nick):
		"""
		Change user nickname
		"""
		if oldnick in self._users:
			self._users[nick] = self._users[oldnick]
			del self._users[oldnick]
			self._users[nick].change_nick(nick)

	def add_user_mode(self, nick, mode):
		"""
		Add a mode to an user
		"""
		if nick in self._users:
			self._users[nick].add_mode(mode)

	def remove_user_mode(self, nick, mode):
		"""
		Remove a mode from an user
		"""
		if nick in self._users:
			self._users[nick].remove_mode(mode)


class Server:
	"""
	IRC Server representation

	Holds all basic client configuration and bot user information

	Patches:
		@since 4.6: Added available modules
	"""
	def __init__(self, host, port, password, encoding, nickname, alternative, username, realname, nickserv):
		self._host = host
		self._name = host
		self._port = port
		self._password = password
		self._encoding = encoding
		self._nickname = nickname
		self._alternative = alternative
		self._current_nickname = nickname
		self._username = username
		self._realname = realname
		self._nickserv = nickserv
		self._channels = {}

		self._quit_msg = ''
		self._part_msg = ''
		self._ssl = False

		self._available_modules = {}

	def clear(self):
		"""
		Clear all channels users from the server object

		@since 5.0
		"""
		for c in self._channels:
			self._channels[c].clear_users()
			debug.info("Server {} Channel {} cleared.".format(
				self._name, c))

	def add_channel(self, name, key):
		"""
		Add a channel to the client
		"""
		self._channels[name] = Channel(self._host, name, key)

	def remove_channel(self, name):
		"""
		Remove a channel from the client
		"""
		if name in self._channels:
			del self._channels[name]

	def add_module(self, moduleid):
		"""
		Add a module to the client

		@since 4.6
		"""
		if not moduleid in self._available_modules:
			self._available_modules.append(moduleid)

	def remove_module(self, moduleid):
		"""
		Remove a module from the client

		@since 4.6
		"""
		if moduleid in self._available_modules:
			self._available_modules.remove(moduleid)

	def has_module(self, moduleid):
		"""
		Returns True if the module is available on this client

		@since 4.6
		"""
		return len(self._available_modules) == 0 or moduleid in self._available_modules

	def get_channel_by_name(self, name):
		"""
		Get a channel by its name
		"""
		debug.debug('Searching for channel ' + name + '...')
		if name in self._channels:
			debug.debug("Channel found " + name)
			return self._channels[name]
		else:
			debug.warn("Channel " + name + " not found !")
			return None

	def is_user_on_channel(self, nickname, channel):
		"""
		Returns True if an user is on a channel, False otherwise
		"""
		if channel in self._channels:
			return self._channels[channel].is_user_present(nickname)

	def quit_user(self, name):
		"""
		Deletes an user from a channel
		"""
		for c in self._channels:
			self._channels[c].remove_user(name)

	def nick_user(self, old, new):
		"""
		Change an user nickname
		"""
		for c in self._channels:
			self._channels[c].change_user_nick(old, new)

	def add_channel_mode(self, channel, mode):
		"""
		Add a mode to a channel
		"""
		if channel in self._channels:
			self._channels[channel].add_mode(mode)

	def remove_channel_mode(self, channel, mode):
		"""
		Remove a mode from a channel
		"""
		if channel in self._channels:
			self._channels[channel].remove_mode(mode)

	def __str__(self):
		"""
		String representation
		Return the client name
		"""
		return self._name

	def get_user_list_of(self, channel, withmode = False):
		"""
		Get a channel user list
		"""
		if channel in self._channels:
			result = []
			for u in self._channels[channel]._users:
				s = self._channels[channel]._users[u]._nick
				if withmode:
					s = s + " ( Modes: " + self._channels[channel]._users[u]._mode + " )"
				result.append(s)
			return result
		else:
			return None

	def channels(self):
		"""
		Generator for channels

		@since 5.0
		"""
		for c in self._channels:
			yield c

	def all_users(self):
		"""
		All users

		@since 5.0
		"""
		for c in self._channels:
			for u in self._channels[c]._users:
				user = self._channels[c]._users[u]
				yield [ user._nick, user._mode ]


class Client(net.IRCClient):
	"""
	Basic IRC client engine

	According to the server configuration, this class holds the whole client configuration
	Holds modules and intercepts events incoming from the network layer
	"""
	def __init__(self, app, config):
		"""
		Inits the base client
		"""
		net.IRCClient.__init__(self)
		self.__config = config

		# Config is a BotConfigServer object
		self.server = Server(
			config.host,
			config.port,
			config.password,
			config.encoding,
			config.userinfo.nickname,
			config.userinfo.alternative,
			config.userinfo.username,
			config.userinfo.realname,
			config.userinfo.nickserv
		)
		self.server._quit_msg = config.quitmsg
		self.server._part_msg = config.partmsg
		self.server._ssl = config.ssl
		self.server._name = config.name
		self.server._available_modules = config.modules
		for c in config.channels:
			self.server.add_channel(c.name, c.key)

		for m in config.modules:
			self.server.add_module(m)

		self.version = config.version % {
			'application_name': app.name,
			'application_version': app.version,
			'application_copyright': app.copyright,
			'application_release': app.release
		}

		self.modules = {}
		self.module_locks = {}

		self.ignorelist = []
		self.mode_symbols = {}

		self.names_timer = timer.BotTimer(60, self.ask_all_names)

		self.initialize()

	def stop(self):
		self.names_timer.stop()

	def is_admin(self, username):
		"""
		Return True if username is a client admin
			according to server configuration
		"""
		return username in self.__config.admins

	def modulecount(self):
		"""
		Get all modules count (active and inactive ones)
		"""
		return len(self.modules.keys())

	def initialize(self):
		"""
		Initializes connection transmitting server configuration
			to the network layer
		"""
		self._connect(self.server._host, self.server._port, self.server._nickname, self.server._password, self.server._username, self.server._realname, ssl = self.server._ssl, encoding = self.server._encoding)

	def register_module(self, name, module):
		"""
		Register a module on this client

		Patches:
			@since 4.6: Register module only if the server configuration doesn't deny it
		"""
		if self.server.has_module(name):
			self.modules[name] = module
		else:
			debug.warn("Module '%s' not loadable on this server." % (name, ))

	def unregister_module(self, name):
		"""
		Unregister a module
		"""
		if name in self.modules:
			del self.modules[name]

	def enable_module(self, name, channel=None):
		"""
		Enable a module on this client
		"""
		if name in self.modules:
			if not self.modules[name].enabled():
				debug.debug("Module %s found." % (name, ))
				if not channel is None:
					debug.debug("  Setting channel '%s' to module %s" % (channel, name))
					self.modules[name].setParam('channel', channel)
				debug.debug("  Enabling...")
				self.modules[name].setEnabled(True)
				if not channel is None:
					debug.debug("  Requesting channel lock...")
					return self.request_channel_lock(name, channel)
				else:
					return True
			else:
				return False
		else:
			debug.debug("Module '%s' not found" % (name, ))
			return False

	def disable_module(self, name):
		"""
		Disable a module on this client
		"""
		if name in self.modules and self.modules[name].enabled():
			self.modules[name].setEnabled(False)
			self.release_lock(self.modules[name], self.modules[name].param('channel'))

	def request_channel_lock(self, module, channel):
		"""
		Request lock on a specified channel for a module
		This denies access on a channel for all other modules
		"""
		if channel in self.module_locks:
			debug.debug("  Channel %s already locked. (%s)" % (channel, self.module_locks[channel]))
			return False
		else:
			self.module_locks[channel] = module
			debug.debug("  Channel successfully locked.")
			return True

	def release_lock(self, module, channel):
		"""
		Release channel lock hold by a module
		"""
		if channel in self.module_locks and self.module_locks[channel] == module.name():
			del self.module_locks[channel]

	def get_lock(self, channel):
		"""
		Identify a module which is currently locking a channel
		"""
		if channel in self.module_locks:
			return self.module_locks[channel]
		else:
			return None

	def on_incoming_message(self, server, event):
		"""
		Dispatch incoming messages from network layers to all registered and active modules
		If a lock is active on the event target (channel), this method does nothing for all other modules
			unless for the module which holds the lock.

		If an event returns false, the event will not be dispatched to remaining modules

		Patches:
			@since 5.2b: Added priority to modules. Less is the number, higher is the priority
		"""
		_mkeys = sorted(self.modules.keys(), key=lambda x: (self.modules[x].priority, x))
		for mn in _mkeys:
			if self.server.has_module(mn):
				module = self.modules[mn]
				if module.enabled():
					lock = self.get_lock(event.target())
					if lock is None or lock == mn:
						if hasattr(module, 'on_' + event.eventtype()):
							if getattr(module, 'on_' + event.eventtype())(self, event) == False:
								break

	def get_user_list(self, chan, withmode = False):
		"""
		Retrieve user list on a channel
		"""
		return self.server.get_user_list_of(chan, withmode)

	def nickname(self):
		"""
		Current bot nickname
		"""
		return self.server._current_nickname

	def ident(self):
		"""
		Current bot username
		"""
		return self.server._ident

	def realname(self):
		"""
		Current bot real name
		"""
		return self.server._realname

	def get_mode_by_symbol(self, symbol):
		"""
		Get mode letter by its symbol.

		According to RFC 1459, the server sends information about user modes
		with the letters and corresponding symbols
		"""
		modeletter = self.mode_symbols.get(symbol)
		if modeletter:
			return modeletter
		else:
			return ''

	def quit(self, message = ''):
		"""
		Send a QUIT message
		"""
		if message == '':
			message = self.server._quit_msg
		self.names_timer.stop()
		net.IRCClient.quit(self, message)

	def part(self, channel, message = ''):
		"""
		Send a PART message on a channel
		"""
		if message == '':
			message = self.server._part_msg
		net.IRCClient.part(self, channel, message)

	def on_version(self, serv, ev):
		"""
		Event triggered on CTCP Version message
		"""
		self.notice(ev.source().nickname, self.version)

	def on_serversupport(self, serv, ev):
		"""
		Event on_serversupport
		Triggered when 005 RPL_SERVERSUPPORT is received

		Event:
			eventtype: 		'serversupport'
			source:			server hostname
			target:			bot nickname
			args:			Key=Value tuples with server support information
		"""
		supportln = ev.arguments()[:-1]
		for sp in supportln:
			kv = sp.split('=')
			if kv[0] == 'PREFIX':
				modestart = 1
				modeend = kv[1].index(')') - 1
				for i in range(modestart, modeend + 1):
					self.mode_symbols[kv[1][i+modeend+1]] = kv[1][i]

	def on_welcome(self, serv, ev):
		"""
		Event on_welcome
		Triggered when 001 RPL_WELCOME is received

		Event:
			eventtype:		'welcome'
			source:			server hostname
			target:			bot nickname
			args:			Welcome message

		This event means the server accepts the client, and allows it to send messages.
		"""
		if len(self.server._nickserv) > 0:
			self.ident()
		for c in self.server._channels:
			debug.debug("--> Try to join {}".format(c))
			chan = self.server._channels[c]
			self.join(chan._name, chan._key)
		self.names_timer.start()

	def on_ping(self, serv, ev):
		"""
		Event on_ping
		Triggered on PING request

		Event:
			eventtype:		'ping'
			source:			server hostname
			target:			None
			args:			Ping identifier

		PING response is automatic. No need to override this method.
		"""
		pass

	def on_pubmsg(self, serv, ev):
		"""
		Event helper for channel PRIVMSG

		Event:
			eventtype:		'pubmsg'
			source:			Sender user object
			target:			Channel
			args:			Message
		"""
		pass

	def on_privmsg(self, serv, ev):
		"""
		Event helper triggered when a private message is sent to the bot

		Event:
			eventtype:		'privmsg'
			source:			Sender user object
			target:			bot nickname
			args:			Message
		"""
		pass

	def on_privnotice(self, serv, ev):
		"""
		Event helper triggered when a notice is sent to the bot

		Event:
			eventtype:		'privnotice'
			source:			sender user object
			target:			bot nickname
			args:			Message
		"""
		pass

	def on_pubnotice(self, serv, ev):
		"""
		Event helper triggered when a notice is sent to a channel

		Event:
			eventtype:		'pubnotice'
			source:			sender user object
			target:			bot nickname
			args:			Message
		"""
		pass

	def on_join(self, serv, ev):
		"""
		Event triggered on JOIN command

		Event:
			eventtype:		'join'
			source:			sender user object
			target:			Can be joined channel, can be None
			args:			Channel joined
		"""
		if ev.source().nickname == self.server._current_nickname:
			channel = ev.target()
			if channel == '':
				channel = ev.arguments()[0]
			debug.debug('New channel: ' + channel)
			self.server.add_channel(channel, '')
		else:
			debug.debug('Received Join Event: ' + str(ev.source()) + ' on ' + ev.arguments()[0])
			self.server.get_channel_by_name(ev.arguments()[0]).add_user(ev.source().nickname, ev.source().ident, ev.source().hostname)

	def on_namreply(self, serv, ev):
		"""
		Event triggered on 353 RPL_NAMREPLY

		Event:
			eventtype:		'namreply'
			source:			server hostname
			target:			bot nickname
			args:			Array(Bot user mode prefix, channel, Space-separated list of users)

		Patches:
			@since 5.0: Clear the server registered users
		"""
		channel = ev.arguments()[1]
		userlist = ev.arguments()[2].strip().split()
		chan = self.server.get_channel_by_name(channel)
		if chan is not None:
			for user in userlist:
				if user[0] in self.mode_symbols:
					chan.add_user(user[1:], user[1:], user[1:], self.get_mode_by_symbol(user[0]))
				else:
					chan.add_user(user, user, user, '')

	def on_part(self, serv, ev):
		"""
		Event triggered on PART command

		Event:
			eventtype:		'part'
			source:			sender user object
			target:			channel
			args:			Part message OR empty
		"""
		self.server.get_channel_by_name(ev.target()).remove_user(ev.source().nickname)

	def on_quit(self, serv, ev):
		"""
		Event triggered on QUIT command

		Event:
			eventtype:		'quit'
			source:			sender user object
			target:			None
			args:			Quit message OR empty
		"""
		debug.info(ev.source().nickname + " has quit.")
		self.server.quit_user(ev.source().nickname)

	def on_invite(self, serv, ev):
		"""
		Event triggered on INVITE command

		Event:
			eventtype:		'invite'
			source:			sender user object
			target:			bot nickname
			args:			channel
		"""
		pass

	def on_nick(self, serv, ev):
		"""
		Event triggered on NICK command

		Event:
			eventtype:		'nick'
			source:			sender user object
			target:			new nickname or None
			args:			new nickname or None
		"""
		if len(ev.arguments()) > 0:
			self.server.nick_user(ev.source().nickname, ev.arguments()[0])
		else:
			self.server.nick_user(ev.source().nickname, ev.target())

	def on_kick(self, serv, ev):
		"""
		Event triggered on KICK command

		Event:
			eventtype:		'kick'
			source:			kicker user object
			target:			channel
			args:			Array(kicked user nickname, reason OR empty)
		"""
		if ev.arguments()[0].lower() != self.server._current_nickname.lower():
			self.server.get_channel_by_name(ev.target()).remove_user(ev.arguments()[0])

	def on_pubaction(self, serv, ev):
		"""
		Event helper triggered on CTCP Action on a channel

		Event:
			eventtype:		'pubaction'
			source:			sender user object
			target:			channel
			args:			Message with ACTION prefix
		"""
		actionmsg = ev.arguments()[0][len('\x01ACTION '):-1]

	def on_mode(self, serv, ev):
		"""
		Event triggered on MODE command

		Event:
			eventtype:		'mode'
			source:			sender user object
			target:			channel
			args:			Array(modes string, targets nicknames OR empty)

		This method parses the modes string to determine how the users modes are modified
		and how the channel modes are set.
		"""
		try:
			channel = ev.target()
			if net.ProtocolMgr._is_a_channel(channel):
				modes = ev.arguments()[0]
				if len(ev.arguments()) > 1:
					targets = ev.arguments()[1:]
				else:
					targets = None

				current_md = ''
				idx = 0
				for m in modes:
					if m == '+' or m == '-':
						current_md = m
					else:
						if targets is None:
							if current_md == '+':
								self.server.add_channel_mode(channel, m)
							elif current_md == '-':
								self.server.remove_channel_mode(channel, m)
						else:
							if current_md == '+':
								self.server.get_channel_by_name(channel).add_user_mode(targets[idx], m)
							elif current_md == '-':
								self.server.get_channel_by_name(channel).remove_user_mode(targets[idx], m)
						idx = idx + 1
		except Exception as ex:
			pass

	def on_nicknameinuse(self, serv, ev):
		"""
		Event triggered on 433 ERR_NICKNAMEINUSE
		This command is sent when the nickname chosen by the bot is already taken.
		This method automatically tries the alternative nickname.

		Event:
			eventtype:		'nicknameinuse'
			source:			server hostname
			target:			'*'
			args:			Array(used nickname, message)
		"""
		self.connection._init_ident(usednick = self.server._alternative)

	def shutdown(self):
		"""
		Shut down the client and asks for disconnect
		"""
		self.connection.disconnect(self.server._quit_msg)

	def ident(self, pwd = None):
		"""
		Send nickserv identification command if nickserv is given
		"""
		if pwd is None:
			if len(self.server._nickserv) != 0:
				self.privmsg('NickServ', 'identify ' + self.server._nickserv)
		else:
			self.privmsg("NickServ", 'identify ' + pwd)

	def ask_all_names(self):
		"""
		Send NAMES request for all registered channels

		@since 5.0
		"""
		self.server.clear()
		for c in self.server.channels():
			self.names(c)
