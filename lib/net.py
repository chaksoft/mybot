#-*- coding:utf-8 -*-
from . import settings
import socket
import ssl
import traceback
import threading
from . import debug
import sys
import asyncio
import multiprocessing
from .error import QuitException


class User:
	"""
	IRC Source User

	This object is NOT for configuration. It represents a real user on IRC
	"""
	def __init__(self, usermask):
		"""
		Initializes the user and parses the user mask
		"""
		self.nickname = ''
		self.ident = ''
		self.hostname = ''
		self.__usermask = usermask[1:]

		if self.__usermask.find('!') != -1:
			self.nickname = self.__usermask.split('!')[0]
			self.ident = self.__usermask.split('!')[1].split('@')[0]
			self.hostname = self.__usermask.split('@')[1]
		else:
			self.hostname = self.__usermask

	def get_mask(self):
		"""
		Gets the original user mask
		"""
		return self.__usermask

	def __str__(self):
		"""
		String representation: User mask
		"""
		return self.get_mask()


class Message:
	"""
	IRC Message

	This object represents an incoming message on the network.
	Parses the line from the received data according to RFC 1459
	"""
	def __init__(self, line, autoparse = True):
		debug.info('Incoming data : ' + line)
		self._incoming = line
		for c in settings.SEPARATOR:
			self._incoming = self._incoming.replace(c, '')

		self.message_type = ''
		self.source = None
		self.target = ''
		self.arguments = []
		self.__command = ''

		if autoparse:
			self.parse()

	def get_command(self):
		"""
		Gets the command part of the line
		"""
		return self.__command

	def __str__(self):
		"""
		String representation of the message for debug output
		"""
		if self.source is None:
			src = "None"
		else:
			src = self.source
		return "[Message object instance] Type: " + str(self.message_type) + " / Source: " + str(src) + " / Target: " + self.target + " / Args: " + str(len(self.arguments))

	def parse(self):
		"""
		Parser

		An IRC Message is formed as:

		[:[nickname!username@]hostname] command [target] [{arguments}] [:message]
		The PING command is the only command which can be sent without any source.

		Command can be an uppercase string or a numeric string command.
		Refer to `settings.py` to see the complete list of numeric commands
		"""
		if len(self._incoming) > 0:
			idx = 0
			parts = self._incoming.split()
			debug.info("Message parsing..." + str(parts))
			if parts[idx].startswith(':'):
				self.source = User(parts[idx])
				idx = idx + 1
			debug.debug("Source : " + str(self.source))
			self.__command = parts[idx]
			debug.debug("Command: " + str(self.__command))
			idx = idx + 1
			if not parts[idx].startswith(':'):
				self.target = parts[idx]
				idx = idx + 1
			rest = parts[idx:]

			debug.debug("Target: " + str(self.target))
			debug.debug("Rest: " + str(rest))
			idx = 0
			for p in rest:
				if p.startswith(':'):
					self.arguments.append(' '.join(rest[idx:])[1:])
					break
				else:
					self.arguments.append(p)
					idx = idx + 1
			debug.debug("Arguments: " + str(self.arguments))


class IRCProtocol(object):
	def __init__(self, connector, reader, writer, loop):
		"""
		IRC Protocol connector

		Manages the reader and the writer on the network
		Uses asyncio Streams API

		@since 5.0
		"""
		self.connector = connector
		self.reader = reader
		self.writer = writer
		self.loop = loop
		self.buffer = ""

	@asyncio.coroutine
	def start(self):
		"""
		AsyncIO Coroutine for receiving data

		This method also handles multiline messages.
		A message on IRC must ends with "CRLF" characters. If not, the message is not terminated.
		A buffer is set to gather incomplete messages and treat them after receiving the last part.
		"""
		running = True
		debug.info("Registering...")
		self.connector._init_ident()
		while running:
			data = yield from self.reader.read(settings.BUFFER_SIZE)
			if len(data) > 0:
				try:
					debug.debug("Received: {}".format(data))
					try:
						buff = str(data, self.connector.encoding())
					except UnicodeDecodeError:
						buff = str(data.decode('latin1').encode(self.connector.encoding()), self.connector.encoding())
					lines = buff.split(settings.SEPARATOR)
					if not buff.endswith(settings.SEPARATOR):
						debug.debug("[!] Incomplete line. Buffering...")
						self.buffer += lines[-1]
						lines = lines[:-1]
					else:
						if self.buffer != '':
							self.buffer += lines[0]
							lines[0] = self.buffer
							self.buffer = ""

					for line in lines:
						ln = line.strip()
						if ln != "":
							m = Message(ln)
							if self.connector.on_incoming:
								self.connector.on_incoming(m)
				except QuitException as exc:
					debug.error("[data_received] {}".format(str(exc)))
					running = False
				except Exception as other_ex:
					pass
			if self.reader.exception() is not None:
				running = False
				debug.error(repr(self.reader.exception()))
		debug.debug("Coroutine @protocol.start is terminated.")
		self.loop.stop()

	@asyncio.coroutine
	def start_write(self):
		"""
		AsyncIO Coroutine for writing data on the network layer

		This method reads a threadsafe Queue to handle messages.
		"""
		debug.debug("[->] start_write")
		try:
			while True:
				data = yield from self.loop.run_in_executor(None, self.connector.stdout.get)
				debug.debug("{} >> {}".format(self.connector.servername(), data))
				self.writer.write(data)
				yield from self.writer.drain()
				debug.debug("     Data sent.")
		except Exception as ex:
			debug.error(repr(ex))
		debug.debug("[<-] start_write")

	def stop(self):
		"""
		Stops writer and kills the start_write coroutine
		"""
		self.writer.close()


class IRCConnector:
	"""
	Main Protocol connector

	Supports TLSv1.2, specifies encoding and creates connection to the network.
	Uses AsyncIO Streams API to manage connections and threadsafe calls

	This class also owns the threadsafe Queue of output messages.

	Patches:
		@since 5.0: Uses AsyncIO
	"""
	def __init__(self, loop, host, port, nickname, password, username, realname, ssl = False, encoding="utf8"):
		"""
		Initializes the connector parameters
		"""
		self.loop = loop
		self.__host = host
		self.__port = port
		self.__nickname = nickname
		self.__password = password
		self.__username = username
		self.__realname = realname
		self.__ssl = ssl
		self.__encoding = encoding

		self.__current_nickname = nickname

		self.on_incoming = None
		self.__socket = None

		self._tmp_line_buffer = ''
		self.protocol = None
		self.stdout = multiprocessing.Queue()

	def servername(self):
		"""
		Returns the server hostname
		"""
		return self.__host

	def encoding(self):
		"""
		Returns server encoding

		@since 5.0
		"""
		return self.__encoding

	def create_protocol(self):
		"""
		Creates connection to the network and starts the protocol routines
		Creates SSL context if SSL is enabled in the configuration.

		Patches:
			@since 5.0: Uses AsyncIO event loop and async calls
		"""
		debug.debug("[->] create_protocol")
		context = False
		if self.__ssl:
			context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
			context.verify_mode=ssl.CERT_NONE
			reader, writer = yield from asyncio.open_connection(self.__host, self.__port, ssl=context)
		else:
			reader, writer = yield from asyncio.open_connection(self.__host, self.__port)
		self.protocol = IRCProtocol(self, reader, writer, self.loop)
		asyncio.async(self.protocol.start_write())
		asyncio.async(self.protocol.start())

	def start(self):
		"""
		Wrapper to start the connection

		@since 5.0
		"""
		debug.info("Connecting to " + self.__host + " on port " + str(self.__port))
		try:
			yield from self.create_protocol()
		except Exception as e:
			debug.error("Disconnected from host. Reason : " + str(e))
			if e is not None:
				self.stop()

	def send_raw(self, raw):
		"""
		Sends RAW message to the network
		This method puts the message in the threadsafe queue, waiting for being treated by the protocol object
		"""
		debug.debug("[->] send_raw")
		rwb = "%s%s" % (raw, settings.SEPARATOR)
		self.stdout.put(bytes(rwb, self.__encoding))
		debug.debug("[*]  send_raw has put {} bytes in queue".format(len(rwb)))
		debug.debug("[<-] send_raw")

	def get_current_nickname(self):
		"""
		Returns the current bot nickname
		"""
		return self.__current_nickname

	def _init_ident(self, usednick = None):
		"""
		Sends initial information to register on the IRC Server

		RFC 1459 says:
			If the server is protected by a password, you must send first:
				PASS <server_password>(CRLF)
			Then you must send the registration information
				NICK <nickname>(CRLF)
				USER <username> <hostname> <client_info> :<realname>(CRLF)

		The client_info is not mandatory and can be any string.
		Some servers can parse this information to gather data, but I never saw that.
		So I choose to put '0'.
		"""
		if usednick is None:
			nickname = self.__nickname
		else:
			nickname = usednick

		self.__current_nickname = nickname
		debug.info("Sending authentication information...")
		if self.__password != '':
			self.send_raw("PASS " + self.__password)
		self.send_raw("NICK " + nickname)
		self.send_raw("USER " + self.__username + " " + self.__host + " 0 :" + self.__realname)

	def stop(self):
		"""
		Stops the server by shutting down the queue and the protocol object
		Calls Asynchronously the stop and close coroutines for shutting down the asyncio loop
		"""
		self.stdout.close()
		if self.protocol is not None:
			self.protocol.stop()
		self.loop.call_soon_threadsafe(self.loop.stop)
		self.loop.call_soon_threadsafe(self.loop.close)

class ProtocolMgr:
	"""
	Protocol Manager

	This static class gets the type of event to be triggered from the received command
	"""
	@staticmethod
	def _is_a_channel(tgt):
		"""
		Static method to determine if the given parameter identifies a channel
		Two characters can define a channel: & and #
		"""
		return tgt.startswith('&') or tgt.startswith('#')

	@staticmethod
	def get_type_from_command(cmd, tgt = '', msg = ''):
		"""
		Static method parsing the command to get an event type
		See `settings.py` to see the complete list of numeric commands
		"""
		debug.debug("Parsing type from command : " + cmd)
		if cmd == 'privmsg':
			if ProtocolMgr._is_a_channel(tgt):
				if msg.startswith('\x01ACTION'):
					return 'pubaction'
				else:
					return 'pubmsg'
			else:
				if msg.startswith('\x01ACTION'):
					return 'privaction'
				elif msg.startswith('\x01VERSION'):
					return 'version'
				else:
					return 'privmsg'

		elif cmd == 'notice':
			if ProtocolMgr._is_a_channel(tgt):
				return 'pubnotice'
			else:
				return 'privnotice'
		elif cmd in settings.NUMERIC_COMMANDS:
			return settings.NUMERIC_COMMANDS[cmd]
		else:
			return cmd


class Event:
	"""
	IRC Event

	This class contains information about an event, and is passed as an argument
	to all event callbacks in clients and modules.
	"""
	def __init__(self, eventtype, source, target, arguments):
		"""
		Initializes the event
		"""
		self.__eventtype = eventtype
		self.__source = source
		self.__target = target
		self.__arguments = arguments

	def eventtype(self):
		"""
		Gets the event type string (determined from ProtocolMgr class)
		"""
		return self.__eventtype

	def source(self):
		"""
		Gets the User object, source of the message
		"""
		return self.__source

	def target(self):
		"""
		Gets the target string
		"""
		return self.__target

	def arguments(self):
		"""
		Gets the arguments of the message
		"""
		return self.__arguments

	def __str__(self):
		"""
		String representation of the message for debug output
		"""
		return "Type: %s / Source: %s / Target: %s / Args: %s" % (self.__eventtype, self.__source, self.__target, self.__arguments)


class IRCClient:
	"""
	IRC Client

	This class owns the connector and parses the incoming message.
	It is inherited by irc.Client to wrap some helper events
	"""
	def __init__(self):
		"""
		Initializes the IRCClient object
		"""
		self.connection = None
		self.loop = asyncio.new_event_loop()
		asyncio.set_event_loop(self.loop)

	def _connect(self, host, port, nickname, password, username, realname, ssl = False, encoding = "utf8"):
		"""
		Creates the connection passing server parameters as arguments
		"""
		self.connection = IRCConnector(self.loop, host, port, nickname, password, username, realname, ssl, encoding)
		self.connection.on_incoming = self._on_incoming_message

	def start(self):
		"""
		Starts connector
		"""
		try:
			self.loop.run_until_complete(self.connection.start())
			self.loop.run_forever()
		except KeyboardInterrupt:
			self.quit("CTRL+C Rage Quit.")
		finally:
			self.loop.close()

	def on_incoming_message(self, server, event):
		"""
		Public incoming message callback
		This method is implemented in children
		"""
		pass

	def _on_incoming_message(self, message):
		"""
		Incoming message callback from connector thread
		Trigger the event corresponding callback from the event type string

		The public on_incoming_message callback is triggered AFTER every other callback.
		"""
		try:
			try:
				if not isinstance(message, Message):
					raise Exception("[Error 001] Unable to parse incoming message. Invalid message type (Expected: csirc.lib.net.Message)")
				cmd = message.get_command().lower()
				tgt = message.target

				if len(message.arguments) > 0:
					result = ProtocolMgr.get_type_from_command(cmd, tgt, message.arguments[0])
				else:
					result = ProtocolMgr.get_type_from_command(cmd, tgt, '')
				message.message_type = result
				ev = Event(message.message_type, message.source, message.target, message.arguments)

				if hasattr(self, '_on_' + result):
					internal = getattr(self, '_on_' + result)
					debug.info("Entering internal event method _on_" + result + "...")
					internal(message.message_type, message.source, message.target, message.arguments)

				if hasattr(self, 'on_' + result):
					event = getattr(self, 'on_' + result)
					debug.info("Entering event method on_" + result + "...")
					event(self.connection, ev)

				self.on_incoming_message(self.connection, ev)
			except Exception as ex:
				if not str(ex).startswith('Closing link'):
					exc_type, exc_value, exc_tb = sys.exc_info()
					exc_ = traceback.format_exception(exc_type, exc_value, exc_tb)
					debug.error("[FATAL] %s" % (exc_[-2].replace("\n", "\t\t"),))
					debug.error("\t\t%s" % (exc_[-1],))

					debug.error("\n".join(exc_))
				raise ex
		finally:
			pass

	def stop(self):
		pass

	def _on_ping(self, mtype, source, target, args):
		"""
		Auto PING answer with PONG
		"""
		pid = target
		if pid == '':
			pid = args[0]
		self.pong(pid)

	def _on_error(self, mtype, source, target, args):
		"""
		Kills connection when an ERROR command is sent

		@since 5.0
		"""
		self.connection.stop()
		self.stop()
		raise QuitException(args[0])

	def _on_kick(self, mtype, source, target, args):
		"""
		Joins back again when the bot is kicked
		"""
		if settings.AUTO_JOIN_KICK and args[0] == self.connection.get_current_nickname():
			self.join(target)

	def privmsg(self, target, message):
		"""
		Helper method to send a privmsg message (private or channel message according to the target)
		"""
		self._raw("PRIVMSG " + target + " :" + message)

	def join(self, channel, key = ""):
		"""
		Helper method to join a channel
		"""
		to_send = "JOIN " + channel
		if key != "":
			to_send = to_send + " " + key
		self._raw(to_send)

	def notice(self, target, message):
		"""
		Helper method to send a notice
		"""
		self._raw("NOTICE " + target + " :" + message)

	def quit(self, message = ""):
		"""
		Helper method to send the QUIT message
		"""
		self._raw("QUIT :" + message)

	def invite(self, nick, channel):
		"""
		Helper method to invite an user on a channel
		"""
		self._raw("INVITE " + nick + " :" + channel)

	def kick(self, channel, nick, reason):
		"""
		Helper method to kick an user from a channel
		"""
		self._raw("KICK " + channel + " " + nick + " :" + reason)

	def mode(self, target, args):
		"""
		Helper method to set a mode to a target
		"""
		self._raw("MODE " + target + " " + args)

	def nick(self, newnick):
		"""
		Helper method to change the bot nickname
		"""
		self._raw("NICK " + newnick)

	def part(self, channel, message = ""):
		"""
		Helper method to send a PART message
		"""
		self._raw("PART " + channel + " :" + message)

	def pong(self, pid):
		"""
		Helper method to send PONG message
		Used for automatic PING response
		"""
		self._raw("PONG :" + pid)

	def action(self, target, message):
		"""
		Helper method to send a CTCP action message
		"""
		self.privmsg(target, "\x01ACTION " + message + "\x01")

	def names(self, target=""):
		"""
		Helper method to send a NAMES message

		@since 5.0
		"""
		self._raw("NAMES {}".format(target))

	def _raw(self, message):
		"""
		Helper method to send a RAW string to the connector
		"""
		self.connection.send_raw(message)
