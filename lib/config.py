#-*- coding:utf-8 -*-

import json, os.path
from . import settings
from datetime import datetime


class BotConfigServer:
	"""
	Bot configuration object
	Gathers information about one server configuration

	Patches:
		@since 4.6: Adding current running modules
	"""
	def __init__(self, name, host, port, encoding, password = '', ssl = False, modules = None):
		self.name = name
		self.host = host
		self.port = port
		self.encoding = encoding
		self.password = password
		self.ssl = ssl
		self.channels = []
		self.userinfo = None
		self.admins = []
		self.quitmsg = ''
		self.partmsg = ''
		self.version = ''
		if modules is None:
			self.modules = []
		else:
			self.modules = modules

class BotConfigChannel:
	"""
	Channel configuration
	"""
	def __init__(self, name, key = ''):
		self.name = name
		self.key = key

class BotConfigUserInfo:
	"""
	Bot user info configuration
	"""
	def __init__(self, nickname, nickserv, alternative, username, realname):
		self.nickname = nickname
		self.nickserv = nickserv
		self.alternative = alternative
		self.username = username
		self.realname = realname

class BotConfigApplication:
	"""
	Bot application configuration
	"""
	def __init__(self, name, version, copyright, release):
		self.name = name
		self.version = version
		self.copyright = copyright
		self.release = release

class BotConfigDatabase:
	"""
	Bot database configuration
	Simple wrapper object
	"""
	def __init__(self, **kwargs):
		for k in kwargs:
			setattr(self, k, kwargs[k])

class BotConfig:
	"""
	Bot configuration wrapper
	Loads information into the configuration file

	Patches:
		@since 5.0: Modify path of config.json
		@since 5.2: Added YAML parsing ability
	"""
	def __init__(self, profile="default", filename="config", **kwargs):
		fmt = kwargs.get('fmt', settings.CONFIGURATION_FORMAT)
		if fmt == "json":
			conffile = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), "conf", "{}.json".format(filename))
			self.__config = json.load(open(conffile, "r", encoding="utf8"))
		elif fmt == "yaml":
			try:
				import yaml
			except ImportError:
				raise Exception("Unable to use YAML parser. Please install 'pyyaml' package in your Python environment or use JSON instead.")
			conffile = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), "conf", "{}.yml".format(filename))
			self.__config = yaml.load(open(conffile, "r", encoding="utf8"))

		if settings.DEBUG:
			ver = self.__config['_application_']['version']
			major, minor, release = ver.split('.')
			self.__config['_application_']['version'] = '%s.%s.%s' % (major, minor, str(int(release) + 1))
			self.__config['_application_']['release'] = datetime.now().strftime("%d/%m/%Y")
			if fmt == "json":
				json.dump(self.__config, open(conffile, "w", encoding="utf8"), indent="\t")
			elif fmt == "yaml":
				yaml.dump(self.__config, open(conffile, "w", encoding="utf8"), default_flow_style=False)

		self.__default_profile = profile
		self.__servers = {}
		self.__application = None
		self.__parse_application()
		self.__parse_db()
		self.__parse_servers()

	def profile(self, name=None):
		"""
		Get profile information from its name
		"""
		if name is None:
			name = self.__default_profile
		return self.__config[name]

	def logger(self):
		"""
		Get selected profile logger
		"""
		return self.profile()['logger']

	def version(self):
		"""
		Get version
		"""
		return self.profile()["version"]

	def server(self, name):
		"""
		Get server information from server name
		"""
		return self.__servers[name]

	def servernames(self):
		"""
		Get list of server names
		"""
		return self.__servers.keys()

	def __server(self, name):
		"""
		Get internal server configuration from server name
		"""
		return self.profile()["configuration"]["servers"][name]

	def application(self):
		"""
		Get application configuration object
		"""
		return self.__application

	def database(self):
		"""
		Get database configuration object
		"""
		return self._db_config

	def __parse_application(self):
		"""
		Parse application configuration from file
		Returns a BotConfigApplication object
		"""
		_app = self.__config['_application_']
		self.__application = BotConfigApplication(_app['name'], _app['version'], _app['copyright'], _app['release'])

	def __parse_db(self):
		"""
		Parse database configuration from file
		Returns a BotConfigDatabase object
		"""
		if self.__config.get('_db_') is None:
			self._db_config = None
		else:
			self._db_config = BotConfigDatabase(**self.__config['_db_'])

	def __parse_servers(self):
		"""
		Parse servers configuration from file
		Returns a BotConfigServer object
		"""
		for nm in self.profile()["configuration"]["servers"]:
			svr = self.__server(nm)
			svrui = svr['userinfo']
			svrch = svr['channels']
			_s = BotConfigServer(nm, svr['host'], svr['port'], svr['encoding'], svr['password'], svr['ssl'], svr.get('modules'))
			_s.quitmsg = svr['quitmsg']
			_s.partmsg = svr['partmsg']
			_s.admins = svr['admins']
			_s.version = svr.get('version', "%(application_name)s %(application_version)s - %(application_copyright)s Release %(application_release)s")
			_s.userinfo = BotConfigUserInfo(svrui['nickname'], svrui['nickserv'], svrui['alternative'], svrui['username'], svrui['realname'])
			for c in svrch:
				_s.channels.append(BotConfigChannel(c['name'], c['key']))
			self.__servers[nm] = _s
