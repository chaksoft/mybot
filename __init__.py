#-*- coding:utf-8 -*-

from .lib.core import BotCore as MyBot
from .lib import module
from .lib import timer
import os

def check_configuration():
	"""
	This method is called on package import to check if configuration files are present
	"""
	_cnf_dir = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))), "conf")
	if os.path.isdir(_cnf_dir):
		if not os.path.isfile(os.path.join(_cnf_dir, "config.json")) and not os.path.isfile(os.path.join(_cnf_dir, "config.yml")):
			raise Exception("Cannot read main configuration file.")
		if not os.path.isfile(os.path.join(_cnf_dir, "modules.json")) and not os.path.isfile(os.path.join(_cnf_dir, "modules.yml")):
			raise Exception("Cannot read modules configuration file.")
	else:
		raise Exception("Cannot find configuration folder.")

def create_folders():
	"""
	Initial folders creation for application structure

	Patches:
		@since 5.2: Removed file creation (user can choose between configuration file formats)
	"""
	_cnf_dir = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))), "conf")
	_mod_dir = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))), "modules")
	_log_dir = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))), "log")
	if not os.path.isdir(_cnf_dir) and not os.path.isdir(_mod_dir) and not os.path.isdir(_log_dir):
		os.mkdir(_cnf_dir)
		os.mkdir(_mod_dir)
		os.mkdir(_log_dir)

if os.environ.get("MYBOT_DYNAMIC", "0") == "0":
	create_folders()
	check_configuration()
