﻿#-*- coding:utf-8 -*-

import datetime
import logging
import logging.config
from . import settings


class Debug:
	"""
	Debug utility static object

	Patches:
		@since 4.4: Debug level management
		@since 5.0: Uses logging Python standard library
					and choice between console stream and rotating file log
	"""
	logger = None

	@staticmethod
	def init(loggername="console", loggerfile=None):
		"""
		Create logger from logging configuration
		"""
		DebugConfig = {
			"handlers": {
				"console": {
					"class": "logging.StreamHandler",
					"formatter": "precise",
					"level": settings.DEBUG_LEVEL,
					"stream": "ext://sys.stdout"
				},
				"file": {
					"class": "logging.handlers.RotatingFileHandler",
					"formatter": "precise",
					"level": settings.DEBUG_LEVEL,
					"filename": "log/mybot.log",
					"maxBytes": 1048576,
					"backupCount": 3
				}
			},
			"formatters": {
				"precise": {
					"format": "[%(asctime)s][%(levelname)-8s] %(name)-15s %(message)s",
					"datefmt": "%d/%m/%Y %H:%M:%S"
				}
			},
			"loggers": {
				"console": {
					'handlers': ['console'],
					'level': settings.DEBUG_LEVEL,
					'propagate': True
				},
				"file": {
					'handlers': ['file'],
					'level': settings.DEBUG_LEVEL,
					'propagate': True
				}
			},
			"disable_existing_loggers": True,
			"version": 1
		}
		if loggerfile is not None and loggername == 'file':
			DebugConfig['handlers']['file']['filename'] = loggerfile
		logging.config.dictConfig(DebugConfig)
		Debug.logger = logging.getLogger(loggername)

	@staticmethod
	def debug(printable):
		"""
		Prints debug information
		"""
		Debug.logger.debug(printable)

	@staticmethod
	def info(printable):
		"""
		Prints info information
		"""
		Debug.logger.info(printable)
			
	@staticmethod
	def warn(printable):
		"""
		Prints a warning information
		"""
		Debug.logger.warning(printable)
			
	@staticmethod
	def error(printable):
		"""
		Prints an error information
		"""
		Debug.logger.error(printable)

def debug(printable):
	"""
	Made for compatibility

	Patches:
		@since 5.0: Calls the Debug object to use logging package
	"""
	Debug.debug(printable)

def info(printable):
	"""
	Made for compatibility

	Patches:
		@since 5.0: Calls the Debug object to use logging package
	"""
	Debug.info(printable)

def warn(printable):
	"""
	Made for compatibility

	Patches:
		@since 5.0: Calls the Debug object to use logging package
	"""
	Debug.warn(printable)

def error(printable):
	"""
	Made for compatibility

	Patches:
		@since 5.0: Calls the Debug object to use logging package
	"""
	Debug.error(printable)
