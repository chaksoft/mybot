#-*- coding:utf-8 -*-

from . import irc, debug, data, event, settings
import sys
import os
import os.path
import modules
import json
import asyncio


class ModuleConfigObject(object):
	def __init__(self, *args, **kwargs):
		self.__keys = []
		for k in kwargs:
			if type(kwargs[k]) == dict:
				setattr(self, k, ModuleConfigObject(**kwargs[k]))
			else:
				setattr(self, k, kwargs[k])
			self.__keys.append(k)

	def get(self, name):
		if name != 'get' and hasattr(self, name):
			return getattr(self, name)
		return None

	def __iter__(self):
		for k in self.__keys:
			yield k


class BotSubModule(event.EventThrower):
	"""
	Sub-module
	Inherits event thrower to send signals to the parent module

	@since 5.0
	"""
	def __init__(self, shortname, parent_module):
		"""
		Initializes the submodule with the parent module and a short name
		"""
		event.EventThrower.__init__(self)
		self.__short_name = shortname
		self.__parent_module = parent_module

	def module(self):
		"""
		Get the parent module
		"""
		return self.__parent_module

	def name(self):
		"""
		Get the submodule short name
		"""
		return self.__short_name

	def debug(self, printable):
		"""
		Helper for print debug string
		"""
		self.module().debug("{%s} %s" % (self.name(), printable))

	def info(self, printable):
		"""
		Helper for print info string
		"""
		self.module().info("{%s} %s" % (self.name(), printable))

	def warn(self, printable):
		"""
		Helper for print warning string
		"""
		self.module().warn("{%s} %s" % (self.name(), printable))

	def error(self, printable):
		"""
		Helper for print error string
		"""
		self.module().error("{%s} %s" % (self.name(), printable))

	def data(self):
		"""
		Get DataManager for the current submodule
		"""
		return getattr(self.__parent_module.data(), self.name())

	def parent_data(self):
		"""
		Get parent module DataManager
		"""
		return self.__parent_module.data()


class BotModule(object):
	"""
	Bot Module base class

	Must be inherited to create a new MyBot module.

	Patches:
		@since 5.0: Includes DataManager for saving internal module data
	"""
	def __init__(self, name, client, priority=None):
		"""
		Initializes the module
		Creates the data manager
		"""
		self.__module_name = name
		self.__lock_channel = False
		self.__enabled = True
		self.__client = client
		self.__params = {}
		if priority is None:
			priority = 99
		self.__priority = priority
		self.__data = data.DataManager(self.__client.database(), self.__module_name)

	def initialize(self):
		"""
		This function can be used to avoid __init__ overriding
		"""
		pass

	@property
	def priority(self):
	    return self.__priority

	def read_conf(self, filename=None):
		"""
		Reads default configuration file

		@since 5.0
		Patches:
			@since 5.0b: Added Module configuration object wrapper
		"""
		if settings.CONFIGURATION_FORMAT == 'json':
			if filename is None:
				filename = "config.json"
			return ModuleConfigObject(**json.load(
				open(
					os.path.join(os.path.dirname(sys.modules[self.__module__].__file__), "conf", filename),
					"r",
					encoding="utf-8"
				)
			))
		elif settings.CONFIGURATION_FORMAT == 'yaml':
			try:
				import yaml
			except ImportError:
				raise Exception("Unable to use YAML parser. Please install 'pyyaml' package in your Python environment or use JSON instead.")
			if filename is None:
				filename = "config.yml"
			return ModuleConfigObject(**yaml.load(
				open(
					os.path.join(os.path.dirname(sys.modules[self.__module__].__file__), "conf", filename),
					"r",
					encoding="utf-8"
				)
			))

	def emit(self, sender, signal, kwargs):
		"""
		Emits a signal from this module

		@since 4.2
		Patches:
			@since 5.0: Uses debug module output to write signal
		"""
		callback = getattr(self, 'on_signal_%s' % (signal,))
		if callback:
			self.debug("[Signal] %s --> %s (%s)" % (sender, self.__module_name, signal))
			return callback(sender, kwargs)
		else:
			return None

	def data(self):
		"""
		Get data manager
		
		@since 5.0
		"""
		return self.__data

	def setParam(self, name, value):
		"""
		Set internal parameter
		"""
		self.__params[name] = value

	def param(self, name):
		"""
		Get an internal parameter from its name
		"""
		return self.__params.get(name)

	def name(self):
		"""
		Get module name
		"""
		return self.__module_name

	def client(self):
		"""
		Get IRCClient instance, parent of the module
		"""
		return self.__client

	def enabled(self):
		"""
		Return True if the module is enabled, False otherwise
		"""
		return self.__enabled

	def setEnabled(self, flag):
		"""
		Set enable flag
		"""
		self.__enabled = flag

	def lock_channel(self):
		"""
		Return True if module needs to lock a channel to run
		"""
		return self.__lock_channel

	def set_lock_channel(self, flag):
		"""
		Set lock channel requesting status
		"""
		self.__lock_channel = flag

	def debug(self, printable):
		"""
		Helper for print debug output

		@since 5.0
		"""
		debug.debug("(%s) %s" % (self.name(), printable))

	def info(self, printable):
		"""
		Helper for print info output
		"""
		debug.info("(%s) %s" % (self.name(), printable))

	def warn(self, printable):
		"""
		Helper for print warning output
		"""
		debug.warn("(%s) %s" % (self.name(), printable))

	def error(self, printable):
		"""
		Helper for print error output
		"""
		debug.error("(%s) %s" % (self.name(), printable))
