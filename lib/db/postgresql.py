#-*- coding:utf-8 -*-

from ._rdbs import RDBSDatabase
try:
	import psycopg2
except ImportError as ie:
	raise Exception("You must install 'psycopg2' package to use PostgreSQL wrapper for MyBot.")


class PostgresqlDatabase(RDBSDatabase):
	"""
	Wrapper for PostgreSQL database interactivity
	Inherits abstract class AbstractDatabase

	NOTE: Database structure is not modified by this wrapper.
	The user has to create the database and the tables before using PostgreSQL with MyBot

	Configuration needed:
		- host: PostgreSQL database server IP address or hostname
		- port: PostgreSQL datbase port
		- user: PostgreSQL username
		- password: PostgreSQL password
		- name: PostgreSQL database name
	"""
	def __init__(self, config):
		super(PostgresqlDatabase, self).__init__(psycopg2, config)
