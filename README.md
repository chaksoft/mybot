# MyBot v5.3

MyBot is a free library which helps you creating your own IRC robot. This library can be easily used with its modules.

## Installation

To use **MyBot**, you will need some prerequisities :

- Python 3.4
- VirtualEnv for Python 3.4
- PiP for Python 3.4

> **Important** : MyBot has not been tested yet on Python 3.5 and could not be fully operational.

To use MyBot in the best conditions, add a Git submodule in your project directory:
```sh
choko@chaksoft:/home/choko/myfirstbot$ git submodule add https://VotreUser@bitbucket.org/chaksoft/mybot.git
Password: 
choko@chaksoft:/home/choko/myfirstbot$
```

If you don't have access rights to the repository, you won't be able to clone it on your local system.

Create your virtualenv :
```sh
choko@chaksoft:/home/choko/myfirstbot$ virtualenv-3.4 ./env
Using base prefix '/usr'
New python executable in env/bin/python3
Also creating executable in env/bin/python
Installing setuptools, pip, wheel...done.
choko@chaksoft:/home/choko/myfirstbot$
```

> On OS X, `virtualenv` is called `pyvenv`.


Activate your virtualenv :
```sh
choko@chaksoft:/home/choko/myfirstbot$ source env/bin/activate
(env)choko@chaksoft:/home/choko/myfirstbot$
```

To create the folder structure of your robot, you can simply start a Python shell and `import mybot`.
```sh
(env)choko@chaksoft:/home/choko/myfirstbot$ python
Python 3.4.3 Interactive shell
>>> import mybot
>>> exit()
```

Application folders are now created and ready to be configured.

----------

## Configuration

You must now configure the bot to reach IRC servers.
By default, MyBot is waiting for JSON configuration files. You can also write them with the YAML definition language.

You must edit the file `conf/config.json` or `conf/config.yml`. If it does not exist, you'll have to create it.

> If you want to use YAML as your configuration format, you will have to install the `pyyaml` package into your virtual environment.
>
> `(env)choko@chaksoft:/home/choko/myfirstbot$ pip install pyyaml`

### Application metadata

#### \_application_

|   |   |
 ---|---|
|`name` |Name of your bot|
|`release` | Date of last release of your bot (updated at each startup)|
|`version` |Version of your bot with `major.minor.build` format. Build number is increased at each startup|
|`copyright` |Your name|


### Profiles

MyBot can run multiple server configurations, called profiles. Each profile is a key in the configuration file, and the key cannot start with an underscore character (`_`).

A default profile can be named `default` and will be loaded if no profile name is passed as an argument to the MyBot core module.

Here is an example of an empty profile section :

```json
"default": {
   "configuration": {
       "servers": {
       }
   },
   "name": "Default profile",
   "logger": {
	   "type": "console"
   }
}
```

```yaml
default:
	configuration:
		servers:
			# ...
	name: Default Profile
	logger:
		type: console
```

In the `servers` object, you will specify your servers options. The specified profile can connect to several servers simulteanously.

#### Servers
Here is an example of a basic server section :
```json
"servers": {
    "WhatEverTheServerName": {
        "host": "1.2.3.4",
        "port": 6667,
        "ssl": false,
        "password": "",
        "partmsg": "The bot is gone!",
        "quitmsg": "The bot is really gone !",
        "encoding": "UTF-8",
        "version": "%(application_name)s %(application_version)s",
        "userinfo": {
	        "nickname": "RoxxorBot",
	        "alternative": "RoxxorBot2",
	        "realname": "Roxxor",
	        "nickserv": "",
	        "username": "Roxxor"
        },
        "admins": [
                "Choko"
        ],
        "channels": [
        ]
    }
}
```

```yaml
servers:
	WhatEverTheServerName:
		host: 1.2.3.4
		port: 6667
		ssl: no
		password: ''
		partmsg: The bot is gone !
		quitmsg: The bot is really gone !
		encoding: UTF-8
		version: '%(application_name)s %(application_version)s'
		userinfo:
			nickname: RoxxorBot
			alternative: RoxxorBot2
			realname: Roxxor
			nickserv: ''
			username: Roxxor
		admins:
			- Choko
		channels:
			# ...
```

#### Admins
You can define the bot admin users by specifying their **usernames** in the `admins` list.


#### Version special value
You can see in the example ahead there is a `version` key with some parameters. The version string is built when the bot started and is transmitted when someone sends a `CTCP VERSION` request to the bot.

- `application_name` : Content of key `name` in section `_application_`.
- `application_version` : Content of key `version` in section `_application`.
- `application_copyright` : Content of key `copyright` in section `_application_`.
- `application_release` : Content of key `release` in section `_application`.

#### Channels
You can make your bot join automatically some channels when it starts up.
Here is an example of a channel list :

```json
"channels": [
	{
		"name": "#SomeWhere",
		"key": ""
	},
	{
		"name": "#AnotherPlace",
		"key": "foobar"
	}
]
```

```yaml
channels:
	- name: '#SomeWhere'
	  key: ''

	- name: '#AnotherPlace'
	  key: foobar
```

If there is no key for the channel, you must leave the `key` value empty.

### Local debug settings
There are 2 available loggers :

|      |       |      |
 ------|-------|------|
| `console` | Console logger | (default)  |
| `file` | Rotating file logger in `log/mybot.log`  |


You can specify you own file path for `file` logger :

```json
"logger": {
	"type": "file",
	"filepath": "/my/path/to/log.log"
}
```

```yaml
logger:
	type: file
	filepath: '/my/path/to/log.log'
```

The `filepath` section is ignored for console logging.

----------
## Modules

MyBot is a modular application. In the `modules` directory, you will write your own modules (each one must have its own directory).

### My first module

First, you will have to register your module in the application.
By default, MyBot will wait for JSON files. As in the bot configuration, you can choose to use YAML.
In the `conf` directory, create a `modules.json` or `modules.yml` file and insert your module :

```json
{
	"myfirst": {
		"class": "MyFirstModule",
		"loaded": true
	}
}
```

```yaml
myfirst:
	class: MyFirstModule
	loaded: yes
```

The key of the object is the _name_ of the module, it will be used by the core to identify the module.
The `loaded` key tells the core to load or not the module at start up. If the value is `false`, there is no way to load the module when the bot is running. You will have to stop your bot, change the value, and restart.

A module is located in its own sub-directory into the `modules` directory, and must have at least the `__init__.py` file and the module core file named `exec.py`.
In this file, you can import every package you want, as far as it has been installed into your virtual environment.
In our example, we create a directory `myfirst` into the `modules` directory, and we can now write the `exec.py` file.

The module requires an object inheriting a special class `BotModule`.
Here is an example :

```python
from mybot import module

class MyFirstModule(module.BotModule):
	pass
```

Here is your first module ! Ok ... so it does nothing.

If you want to add some properties into your module, you can overload the constructor :

```python
# The class name must be the one you've declared in 'modules.json' or 'modules.yml'
class MyFirstModule(module.BotModule):
	def __init__(self, name, client):
		super(MyFirstModule, self).__init__(name, client)
		self.foo = "bar"
```

- `client` is the wrapper to send message over the network to the IRC server.
- `name` is the name you've given to your module in the modules configuration file. (here it will be `myfirst`.

#### Module priority

You can specify in the modules configuration file a priority.
Lower is the priority number, higher is the module priority. The MyBot core will load modules in the priority order. If you did not specify an order, modules will be loaded by alphabetical order.

Example:
```json
{
	"myfirst": {
		"class": "MyFirstModule",
		"loaded": true,
		"priority": 10
	}
}
```

```yaml
myfirst:
	class: MyFirstModule
	loaded: yes
	priority: 10
```

### Events

Each module can receive events from IRC messages. Every IRC message can be intercepted by its short name. Some helpers have been developed to separate private and public channel messages, or actions (_/me_ messages).

You can retrive the complete event and helpers list at [the end of this document](#event-list).
To handle an event, you just have to create a method in your module named `on_<eventtype>`. As usual, an example :

Assume you want your bot to say _Hello!_ as a private message to everyone who joins one of the channels where the bot is present.

```python
def on_join(self, server, ev):
	serv.privmsg(ev.source().nickname, "Hello!")
```

The event method got 2 arguments :

|     |      |
 ---- | ---- |
|  `serv`  | Wrapper of server which can send messages through the network (same object as `self.client()` from a module instance). |
| `ev` | Event information |

#### Event Object
An event object has 4 getters :

|                  |                              |
 ----------------- | ---------------------------- |
|  `eventtype()`     |  Name of the thrown event    |
|  `source()`        |  Origin of the event (user)  |
|  `target()`        |  Channel or user where the event has been thrown|
|  `arguments()`     |  Parameters of the event (message)    |


#### Source object
An event source is identified by its nickname, username, and hostname. If the event has been raised by a server message, there is only a hostname.

|                  |                              |
 ----------------- | ---------------------------- |
|  `nickname`      |  Nickname of the source (empty string if source is not an user)    |
|  `ident`         |  Username of the source (empty string if source is not an user)      |
|  `hostname`      |  Hostname of the source      |



### Debug

MyBot produces a lot of logs, but you can write your own by using helper methods.

|                  |                              |
 ----------------- | ---------------------------- |
|  `debug(str)`    |  Print debug log     |
|  `info(str)`     |  Print info log      |
|  `warn(str)`     |  Print warning log   |
|  `error(str)`    |  Print error log   |

In your module, when you want to print some logs, you just have to :
```python
def on_join(self, server, ev):
	serv.privmsg(ev.source().nickname, "Hello!")
	self.info("{} has just joined {} !".format(
		ev.source().nickname, ev.target())
```

If the user `Foo` joined the `#Bar` channel, you will see this in the logs :

```
[30/09/2015 18:00:21][Info] Foo has just joined #Bar !
```

----------

## Advanced module management
### Enable / Disable your module at runtime

You can enable or disable a module at runtime by calling the `enable_module` method or the `disable_module` method of the `client` property or the event callback `server` argument.

Example:
```python
def on_join(self, server, ev):
	server.enable_module('mysecond')
```

`enable_module(name, channel=None)`
:     `name` must be the module identifier in `modules.json` / `modules.yml` configuration file.
:      If you give a channel to this method, no other module could interact on the channel until the lock is released.

`disable_module(name)`
:     `name` must be the module identifier in `modules.json` / `modules.yml` configuration file.
:     If the module is already disabled, this method does nothing.

----------

## Databases

You will surely want to store data from your modules. MyBot gives you some helpers and wrappers to write into some common databases :
In MyBot, there are 3 available databases :

| Engine name in config | Python package needed  | Docs |
 ------|------|
 | `mongo` | `pymongo` | [MongoDB database](https://www.mongodb.org/) |
 | `mysql` | `MySQLdb` | [MySQL database](http://www.mysql.com/) |
 | `postgresql` | `psycopg2` | [PostgreSQL database](http://www.postgresql.org/) |

You will need to install manually the needed package for the database you've chosen.
These packages are all available on [PyPi](https://pypi.python.org/pypi) and installable via `pip`.

The database is configured in the `config.json` / `config.yml` file, in a `_db_` section (sibling of the `_application_` section).

```json
"_db_": {
	"engine": "mysql",
	"host": "1.2.3.4",
	"port": 3306,
	"user": "myfirstbot",
	"password": "myfirstbotpass",
	"name": "myfirstbot"
}
```

```yaml
_db_:
	engine: mysql
	host: 1.2.3.4
	port: 3306
	user: myfirstbot
	password: myfirstbotpass
	name: myfirstbot
```

> **Tip** : You can download an example file of JSON configuration file [here](https://bitbucket.org/chaksoft/mybot/downloads/config.json.sample).
> **Tip** : You can download an example file of YAML configuration file [here](https://bitbucket.org/chaksoft/mybot/downloads/config.yml.sample).
> If you don't want to use a database in your robot, don't add the `_db_` section.

### Data through a module
Each module has a `data()` method returning a `DataManager` object. This is a helper which can make basic requests on data.

Every data record is wrapped in a `DataObject` instance, this object containing all fields of the record.

#### DataManager object

`create_object(name, cls=DataObject, **kwargs)`
:   Create an empty `DataObject` with a mandatory name (table) and data arguments. Each argument name must be the name of a field in the database.
:   If the `cls` parameter is given, the data manager will create an object of the given class.
:   **Warning**: the class given as an argument **must extend** the `DataObject` class.
	
`get(name, sort=None, cls=DataObject, limit=None, **criteria)`
:   Get records according to the name of the table, sorting parameters and search criteria as a list of arguments.
:   The sorting argument must be a list of tuples, each tuple must be a couple `(str, int)` where the first argument is the name of the field, and the second is `-1` for descending sorting and `1` for ascending sorting.
:   If the `cls` parameter is given, the data manager will create an object of the given class.
:   The `limit` argument specifies how many results at maximum the method will fetch.
:   **Warning**: the class given as an argument **must extend** the `DataObject` class.

`get_one(name, sort=None, cls=DataObject, **criteria)`
:     Same as `get` but returns only the first object of the result set.
:   If the `cls` parameter is given, the data manager will create an object of the given class.
:   **Warning**: the class given as an argument **must extend** the `DataObject` class.

`save(_obj)`
:    Save a `DataObject` into the database.

`delete(name, **criteria)`
:    Delete a record from the table `name` and according to the given criteria.

`get_count(name, **criteria)`
:     Get the records count in the table `name` according to the given criteria.

`get_max(name, key, **criteria)`
:     Get the object in the table `name` where the value of the field `key` is the max according to the given criteria.

`get_min(name, key, **criteria)`
:     Get the object in the table `name` where the value of the field `key` is the min according to the given criteria.

`get_random(name, cls=DataObject, **criteria)`
:     Get a random object in the table `name` according to the given crtieria.
:   If the `cls` parameter is given, the data manager will create an object of the given class.
:   **Warning**: the class given as an argument **must extend** the `DataObject` class.


#### Store data
When you will have to store some data in your database, you will have to create an object and feed it in your module.

For example, here we will save every join in the channel `#foo` :
```python
def on_join(self, server, ev):
	if ev.source().nickname != self.client().nickname():
		if ev.target() == '#foo':
			self.data().save(
				self.data().create_object('joinmodule',
					nick=ev.source().nickname,
					channel=ev.target(),
					jointime=datetime.now().timestamp()
				)
			)
```

When it's done, the bot will save data in a table named `myfirst_joinmodule` with the fields `nick`, `channel` and `jointime`.

> **Note**
> The tables are named with the lowercase module name and the lowercase object collection name separated by an underscore.

> **Warning**
> You cannot name a field `_name_` in your database. This is the internal name of the collection name argument.

----------

## Run the bot
Now, you're ready to run your bot !

You must create the entry point module of your application like this :

```python
from lib import MyBot
MyBot().start()
```

If you've chosen to write configuration files in `yaml` format, you must specify this format in the `MyBot` constructor :

```python
from lib import MyBot
MyBot(configuration_format="yaml").start()
```

> **Note**:
> Feel free to call the core starter as you want, with exception handling or arguments parsing, for example.

----------

## Appendix

### Event List

#### Named commands
For each named command, the event type is the **lowercase** command string. For example, for the command `NICK`, the event type will be `nick`.

#### Helpers
There are also some helper events which have been developed to make the code easier to maintain :

 |                     |                      |
 ----------------------|----------------------|
 | `pubmsg`            | Event triggered when a message arrives on a channel |
 | `privmsg`     |  Event triggered when a private message is sent to the bot |
 | `pubaction`     | Event triggered when a `/me` action is sent on a channel |
 | `privaction`   | Event triggered when a `/me` action is sent in a private message to the bot |
 | `pubnotice` | Event triggered when a notice is sent to an entire channel |
 | `privnotice` | Event triggered when a notice is directly sent to the bot |
 | `version` | Event triggered when a CTCP version is sent to the bot.

#### Numeric commands

| Numeric RFC code |  Event type in MyBot         |                   |
 ----------------- | ---------------------------- |-------------------|
|`001` | `welcome` |
|`002` | `serververinfo` |
|`003` | `servercreatdate` |
|`004` | `servermodes` |
|`005` | `serversupport` |
|`200` | `tracelink` |
|`201` | `traceconnecting` |
|`202` | `tracehandshake` |
|`203` | `traceunknown` |
|`204` | `traceoperator` |
|`205` | `traceuser` |
|`206` | `traceserver` |
|`208` | `tracenewtype` |
|`209` | `traceclass` |
|`211` | `statslinkinfo` |
|`212` | `statscommands` |
|`213` | `statscline` |
|`214` | `statsnline` |
|`215` | `statsiline` |
|`216` | `statskline` |
|`217` | `statsqline` |
|`218` | `statsyline` |
|`219` | `endofstats` |
|`221` | `umodels` |
|`231` | `serviceinfo` |
|`232` | `endofservices` |
|`233` | `service` |
|`235` | `servlistend` |
|`241` | `statssline` |
|`242` | `statsuptime` |
|`243` | `statsoline` |
|`244` | `statshline` |
|`251` | `luserclient` |
|`252` | `luserop` |
|`253` | `luserunknown` |
|`254` | `luserchannels` |
|`255` | `luserme` |
|`256` | `adminme` |
|`257` | `adminloc1` |
|`258` | `adminloc2` |
|`259` | `adminemail` |
|`261` | `tracelog` |
|`300` | `none` |
|`301` | `away` |
|`302` | `userhost` |
|`303` | `ison` |
|`305` | `unaway` |
|`306` | `noaway` |
|`311` | `whoisuser` |
|`312` | `whoisserver` |
|`313` | `whoisoperator` |
|`314` | `whowasuser` |
|`315` | `endofwho` |
|`316` | `whoischanop` |
|`317` | `whoisidle` |
|`318` | `endofwhois` |
|`319` | `whoischannels` |
|`321` | `liststart` |
|`322` | `list` |
|`323` | `listend` |
|`324` | `channelmodeis` |
|`331` | `notopic` |
|`332` | `topic` |
|`341` | `inviting` |
|`342` | `summoning` |
|`351` | `version` |
|`352` | `whoreply` |
|`353` | `namreply` |
|`361` | `killdone` |
|`362` | `closing` |
|`363` | `closeend` |
|`364` | `links` |
|`365` | `endoflinks` |
|`366` | `endofnames` |
|`367` | `banlist` |
|`368` | `endofbanlist` |
|`369` | `endofwhowas` |
|`371` | `info` |
|`372` | `motd` |
|`373` | `infostart` |
|`374` | `endofinfo` |
|`375` | `motdstart` |
|`376` | `endofmotd` |
|`381` | `youreoper` |
|`382` | `rehashing` |
|`384` | `myportis` |
|`391` | `time` |
|`392` | `usersstart` |
|`393` | `users` |
|`394` | `endofusers` |
|`395` | `nousers` |
|`401` | `nosuchnick` |
|`402` | `nosuchserver` |
|`403` | `nosuchchannel` |
|`404` | `cannotsendtochan` |
|`405` | `toomanychannels` |
|`406` | `wasnosuchnick` |
|`407` | `toomanytargets` |
|`409` | `noorigin` |
|`411` | `norecipient` |
|`412` | `notexttosend` |
|`413` | `notoplevel` |
|`414` | `wildtoplevel` |
|`421` | `unknowncommand` |
|`422` | `nomotd` |
|`423` | `noadmininfo` |
|`424` | `fileerror` |
|`431` | `nonicknamegiven` |
|`432` | `erroneusnickname` |
|`433` | `nicknameinuse` |
|`436` | `nickcollision` |
|`441` | `usernotinchannel` |
|`442` | `notonchannel` |
|`443` | `useronchannel` |
|`444` | `nologin` |
|`445` | `summondisabled` |
|`446` | `usersdisabled` |
|`451` | `notregistered` |
|`461` | `needmoreparams` |
|`462` | `alreadyregistered` |
|`463` | `nopermforhost` |
|`464` | `passwdmismatch` |
|`465` | `yourebannedcreep` |
|`466` | `youwillbebanned` |
|`467` | `keyset` |
|`471` | `channelisfull` |
|`472` | `unknownmode` |
|`473` | `inviteonlychan` |
|`474` | `bannedfromchan` |
|`475` | `badchannelkey` |
|`476` | `badchanmask` |
|`481` | `noprivileges` |
|`482` | `chanoprivsneeded` |
|`483` | `cantkillserver` |
|`491` | `nooperhost` |
|`492` | `noservicehost` |
|`501` | `umodeunknownflag` |
|`502` | `usersdontmatch` |

> **Note**:
Some of these events are never triggered in the client-to-server protocol context.


----------

## License
This software is under ChakSoft software license.
You cannot redistribute this software without the written agreement of the editor. You can use and edit the code of this software for **your personal use only**.

Copyright © 2015 Michael Chacaton. All Rights reserved.