#-*- coding:utf-8 -*-

from .config import BotConfig
from .client import BotClient
from .debug import Debug
from . import settings
from multiprocessing import Process


class BotCore:
	"""
	Main core application

	This object loads configuration and controls clients connected to configured servers in the given profile.
	Patches:
		@since 5.0: Adding settings override from constructor and logger initialization
		@since 5.0b: Use asyncio to create loop here. It's necessary to handle multi servers
	"""
	def __init__(self, **kwargs):
		self.settings = {}
		for k in kwargs:
			self.set_setting(k, kwargs[k])
		self.__config = BotConfig(**kwargs)
		self.clients = {}
		Debug.init(loggername=self.__config.logger()['type'], loggerfile=self.__config.logger().get('filepath'))

	def start(self):
		"""
		Start all configured clients
		"""
		procs = []
		for nm in self.__config.servernames():
			procs.append(Process(target=self.startOne, args=(nm,)))
		try:
			for p in procs:
				p.start()
			for p in procs:
				p.join()
		except KeyboardInterrupt:
			pass
		finally:
			pass

	def startOne(self, name):
		"""
		Start one client from its name
		"""
		self.clients[name] = BotClient(self, self.__config.application(), self.__config.server(name), database=self.__config.database())
		self.clients[name].start()

	def stop(self):
		"""
		Stop all clients
		"""
		for nm in self.__config.servernames():
			self.stopOne(nm)
		self.loop.close()

	def stopOne(self, name):
		"""
		Stop one client from its name
		"""
		self.clients[name].quit()
		del self.clients[name]

	def restartOne(self, name):
		"""
		Restart a client from its name
		"""
		self.stopOne(name)
		self.startOne(name)

	def restart(self):
		"""
		Restart clients by stopping them all and restarting all
		"""
		self.stop()
		self.start()

	def set_setting(self, name, value):
		"""
		Override setting by giving a name and a value

		@since 5.0
		"""
		setattr(settings, name.upper(), value)
