#-*- coding:utf-8 -*-

import time
import struct
import socket
import hashlib
import os
import random
import binascii


def generate_oid():
	"""
	Generate an object ID with the MongoDB algorithm
	"""
	def _machine_bytes():
		machine_hash = hashlib.md5()
		machine_hash.update(socket.gethostname().encode())
		return machine_hash.digest()[:3]

	oid = struct.pack(">i", int(time.time()))
	oid += _machine_bytes()
	oid += struct.pack(">H", os.getpid() % 0xFFFF)
	oid += struct.pack(">i", random.randint(0, 0xFFFFFF))[1:4]

	return binascii.hexlify(oid).decode()