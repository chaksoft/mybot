#-*- coding:utf-8 -*-

from ._rdbs import RDBSDatabase
try:
	import MySQLdb
except ImportError as ie:
	raise Exception("You must install 'MySQLdb' package to use MySQL wrapper for MyBot.")


class MysqlDatabase(RDBSDatabase):
	"""
	Wrapper for MySQL database interactivity
	Inherits abstract class AbstractDatabase

	NOTE: Database structure is not modified by this wrapper.
	The user has to create the database and the tables before using MySQL with MyBot

	Configuration needed:
		- host: MySQL database server IP address or hostname
		- port: MySQL datbase port
		- user: MySQL username
		- password: MySQL password
		- name: MySQL database name
	"""
	def __init__(self, config):
		super(MysqlDatabase, self).__init__(MySQLdb, config)
