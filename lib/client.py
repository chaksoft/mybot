#-*- coding:utf-8 -*-

from .irc import Client
from . import module
from . import debug
import modules
import json, os.path
from . import settings


class BotClient(Client):
	"""
	Inherits from irc.Client class
	Modules loading and database initialization

	Patches:
		@since 4.0: Module list read from subpackages
		@since 4.1: Module list in modules.json file
		@since 5.0: Includes database configuration
	"""
	def __init__(self, core, app, config, database=None):
		Client.__init__(self, app, config)
		self.__core = core
		self.__module_config = {}
		self.__db = database
		self.initialize_modules()

	def database(self):
		"""
		Getter for database configuration

		@since 5.0
		"""
		return self.__db

	def request_stop(self):
		"""
		Request bot stop
		"""
		self.__core.stopOne(self.server._name)

	def request_restart(self):
		"""
		Request bot restart
		"""
		self.__core.restartOne(self.server._name)

	def initialize_modules(self):
		"""
		Modules loading

		Patches:
			@since 4.1: Reading 'modules.json' file in configuration folder
			@since 5.0: Modify structure for deployment, read file in a upper folder level
			@since 5.2: Added YAML parser ability
		"""
		if settings.CONFIGURATION_FORMAT == 'json':
			self.__module_config = json.load(
				open(
					os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), "conf", "modules.json"
				),
				"r",
				encoding="utf-8")
			)
		elif settings.CONFIGURATION_FORMAT == 'yaml':
			try:
				import yaml
			except ImportError:
				raise Exception("Unable to use YAML parser. Please install 'pyyaml' package in your Python environment or use JSON instead.")
			self.__module_config = yaml.load(
				open(
					os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), "conf", "modules.yml"),
					"r",
					encoding="utf8"
				)
			)
		for mod in self.__module_config:
			self.load_module(mod)

	def load_module(self, mod):
		"""
		Load a module from its configuration

		@Patches
			@since 4.6: Load the module only if the server is allowed to run it
		"""
		if self.server.has_module(mod):
			cfg = self.__module_config[mod]
			mcls = cfg['class']
			mload = cfg['loaded']
			if mload:
				debug.debug("Registering module %s..." % (mod, ))
				exec('from modules.%(mod)s import exec as %(mod)s' % { 'mod': mod })
				modulecls = getattr(locals()[mod], mcls)
				imodule = modulecls(mod, self, priority=cfg.get('priority'))
				self.register_module(mod, imodule)
				imodule.initialize()
				debug.debug("    Module %s registered successfully." % (mod, ))
			else:
				debug.debug("Ignoring module %s" % (mod, ))

	def signal_module(self, sender, module, signal, **kwargs):
		"""
		Emit a signal to a specified module

		@since 4.2
		"""
		if module in self.modules:
			return self.modules[module].emit(sender, signal, kwargs)
		else:
			return None

	def privmsgall(self, message, nickname = None):
		"""
		Send a PRIVMSG message

		If `nickname` is None, the message is sent on all channels
		Otherwiser, the message is sent on the channels where `nickname` is present
		"""
		for chan in self.server._channels:
			if not chan is None and chan.strip() != '':
				if nickname is None or self.server._channels[chan].is_user_present(nickname):
					self.privmsg(chan, message)
