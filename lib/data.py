#-*- coding:utf-8 -*-

from . import debug, database

class DataList(list):
	"""
	Wrapper for data list representation
	Built from a args

	@inherit Built-in `list` object
	"""
	def __init__(self, *args):
		list.__init__(self)
		for _w in args:
			if type(_w) == dict:
				self.append(DataObject(**_w))
			elif type(_w) == list:
				self.append(DataList(*_w))
			else:
				self.append(_w)

	def to_json(self):
		"""
		JSON object transformer
		"""
		_j = []
		for item in self:
			if type(item) in (DataObject, DataList):
				_j.append(item.to_json())
			else:
				_j.append(item)
		return _j



class DataObject(object):
	"""
	Wrapper for data representation
	Built from a kwargs
	"""
	def __init__(self, **kwargs):
		self.__keys = []
		for k in kwargs:
			if type(kwargs[k]) == dict:
				self.add(k, DataObject(**kwargs[k]))
			elif type(kwargs[k]) == list:
				self.add(k, DataList(*kwargs[k]))
			else:
				self.add(k, kwargs[k])

	def get(self, name):
		if name not in self.__keys:
			raise Exception("Key {} not found.".format(name))
		return getattr(self, name)

	def id(self):
		try:
			return self._id
		except AttributeError:
			return None

	def add(self, name, value, cls=None):
		if cls is None:
			cls = DataObject
		if name.startswith("__"):
			name = name[2:]
		if type(value) == dict:
			setattr(self, name, cls(**value))
		elif type(value) == list:
			setattr(self, name, DataList(*value))
		else:
			setattr(self, name, value)
		self.__keys.append(name)

	def remove(self, name):
		delattr(self, name)
		self.__keys.remove(name)

	def keys(self):
		for k in self.__keys:
			yield k

	def to_json(self):
		_j = {}
		for k in self.keys():
			_j[k] = self.get(k)
			if type(_j[k]) == DataObject:
				_j[k] = _j[k].to_json()
			elif type(_j[k]) == DataList:
				_j[k] = _j[k].to_json()
		return _j


class DataManager:
	"""
	Main Data Manager

	Each module owns an instance of this object
	This object is the main wrapper for database querying
	"""
	def __init__(self, dbconfig, module_name):
		self.transformer = database.DataTransformer(dbconfig)
		self.module = module_name
		self.dataclass = None

	def create_object(self, _name_, cls=DataObject, *args, **kwargs):
		"""
		Creates an empty DataObject
		Fills it up with the given parameters
		"""
		_obj = cls(**kwargs)
		for arg in args:
			_obj.add(arg, None)
		_obj.add('_name_', _name_)
		return _obj

	def save(self, _obj):
		"""
		Saves the object into the database
		"""
		return self.transformer.save_object(self.module, _obj)

	def delete(self, _name_, **criteria):
		"""
		Deletes an object from the database
		"""
		self.transformer.delete_object(self.module, _name_, **criteria)

	def get(self, _name_, sort=None, cls=DataObject, limit=None, **criteria):
		"""
		Gets objects from the database
		"""
		items = self.transformer.get_objects(self.module, _name_, sort=sort, **criteria)
		if limit is not None:
			items = items[:limit]
		for item in items:
			yield cls(**item)

	def get_one(self, _name_, sort=None, cls=DataObject, **criteria):
		"""
		Gets one object from the database
		"""
		item = self.transformer.get_objects(self.module, _name_, one=True, sort=sort, **criteria)
		if item is None:
			return None
		return cls(**item)

	def get_count(self, _name_, **criteria):
		"""
		Gets objects count from the database
		"""
		return self.transformer.get_object_count(self.module, _name_, **criteria)

	def get_max(self, _name_, key, **criteria):
		"""
		Gets max key object from the database
		"""
		return self.get_one(_name_, sort=[(key, -1)], **criteria)

	def get_min(self, _name_, key, **criteria):
		"""
		Gets min key object from the database
		"""
		return self.get_one(_name_, sort=[(key, 1)], **criteria)

	def get_random(self, _name_, cls=DataObject, **criteria):
		"""
		Gets random object from the database
		"""
		item = self.transformer.get_random(self.module, _name_, **criteria)
		if item is None:
			return None
		return cls(**item)
